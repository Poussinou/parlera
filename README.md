# Parlera

Parlera is an open-source word guessing party game.

## Join the conversation

If you want to help out or even just ask a question, you can reach the community at https://matrix.to/#/#parlera:matrix.org .

## Contribute

You can help through:

- **Programming.** Just take an [issue](https://gitlab.com/enjoyingfoss/parlera/-/issues) and assign it to yourself. If you want to work on a feature that doesn't have an associated issue yet, report a new issue first.
- **Translating.** Go to [Weblate](https://hosted.weblate.org/projects/parlera/) and choose or add your language.

### Words you can submit

When you submit entries, make sure that they are:

* Known by the most adult native speakers of a specific language (for country-specific words, use the *Region* column)
* In their most basic form — just like dictionary entries (i.e. singular, first person, infinitive, ...)
* Family-friendly
* Not derogatory
* Not originally from works under copyright (e.g. "Harry Potter" would not be accepted)

Contributions might not be accepted even if they meet the rules here.

## Install

[<img src="https://f-droid.org/badge/get-it-on.png"
      alt="Get Parlera on F-Droid"
      height="108">](https://f-droid.org/packages/com.enjoyingfoss.parlera/)
[<img src="https://play.google.com/intl/en_us/badges/images/generic/en-play-badge.png"
      alt="Get Parlera on Google Play"
      height="108">](https://play.google.com/store/apps/details?id=com.enjoyingfoss.parlera)
[<img src="https://www.flathub.org/assets/badges/flathub-badge-en.png" alt="Download Parlera Flatpak on Flathub" height="72">](https://flathub.org/apps/details/com.enjoyingfoss.Parlera)

Coming to F-Droid [as soon as it's packaged](https://gitlab.com/fdroid/rfp/-/issues/1986).
